import boto3

def insert_user_into_dynamo(user,tableName):
    dynamodb = boto3.resource("dynamodb")
    table = dynamodb.Table(tableName)
    table.put_item(Item={"id": user["UserId"], "name": user["name"], "email": user["email"]})