from flask import Flask , request
from validation import UserSchema
from database import insert_user_into_dynamo

app = Flask(__name__)


@app.route("/")
def hello():
    return "Hello World!"

@app.route("/goodBye")
def goodbye():
    return "Good Bye World!"

@app.route("/addUser",methods=['POST'])
def addUser(): 
    if request.method == 'POST': 
        errors = UserSchema().validate(request.get_json())

        if errors :
            return {'message': errors},400 
        
        insert_user_into_dynamo(request.get_json(),'User')
        return {'message':'User add to the database'} , 200


if __name__ == "__main__":
    app.run(host="0.0.0.0")
    insert_user_into_dynamo({"UserId":1,"name":'marius',"email":'mariusV@gmail.com'},'UserDB')
    insert_user_into_dynamo({"UserId":2,"name":'bruno',"email":'brunoD@gmail.com'},'UserDB')
