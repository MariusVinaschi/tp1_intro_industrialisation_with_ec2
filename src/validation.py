from marshmallow import Schema , fields, validate , validates 

class UserSchema(Schema): 
    userId = fields.Integer(required=True)
    name = fields.String(required=True, validate=[validate.Length(1,50)])
    email = fields.Email(required=True)