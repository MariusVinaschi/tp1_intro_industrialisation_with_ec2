variable "region" {
  description = "The aws region the resources will be build"
  type        = string
  default     = "eu-west-1"
}

variable "myKeyName" {
  description = "key Name"
  type        = string
  default     = "mySecondKeyName"
}

variable "instance_type" {
  description = "aws ec2 instance type"
  type        = string
  default     = "t2.micro"
}

variable "tag_name" {
  description = "Aws tag name permit to search an instance by tag"
  type        = string
  default     =  "mySecondWebInstance"
}

variable "aws_public_key_ssh_path" {
  description = "The key name of the Key Pair to use for the instance"
  type        = string
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCWUZ1uDBtFeG7TUY29OXH5I4JI47oA/H4MsXckkU7eF/Bmz2/NhX6ZNbYqvfkvXPrjAvmRYLDXeikZZubGfLW84r8Ee+IurZmz79hxRKd9TuqPrAaLVXaAInIXUGULZj9L2LzNt2KUa2Em8wNDcr5Px2XZf/rcV4DI87e4EsnGnNBrPmEqeaHxnb3vOFS+YOrDEJns+86jqwrbMo8tzbSNfbaaf5/uO5PhdCTg2gm0RWy6ogRF9dAKnlOIab15Nk7LLroO8LsTCBEfSoRY5uzU894wfPad4lp6SSzPSawX6KCEGXuU7UcCmqnhtLUO0AfLIj/b7dcOc3h631FvHcgNErQV4eOg7r1t2FHYEUYGAEq/ZejB0gR1rrzbXB+tgrS/HXeLxnmG36uGOdUicZlgYR8fBrnOgXI/zfRHDcc19bItZ5ysXD5DeeC3lQu594RFiLfviXhL9E1jJh3y/vCP5nKRUfEx9wZcrMo0JzV46GfwAEhSlSXtXz4B0Azd7z8= marius@pop-os"
}

variable "ami_id" {
    description = "id of an ami by default it's ubuntu 16.04"
    type        = string
    default     = "ami-03ef731cc103c9f09"
}

