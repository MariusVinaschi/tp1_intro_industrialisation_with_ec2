resource "aws_dynamodb_table" "User-table" {
  name           = "UserDB"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "UserId"

  attribute {
    name = "UserId"
    type = "N"
  }
}


resource "aws_iam_role" "role-dynamodb" {
  name = "dynamodb-role"

  assume_role_policy = <<EOF
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": "sts:AssumeRole",
          "Principal": {
            "Service": "ec2.amazonaws.com"
          },
          "Effect": "Allow",
          "Sid": ""
        }
      ]
    }
EOF
}

resource "aws_iam_policy" "policy-dynamodb" {
  name        = "dynamodb-policy"
  description = "A policy for the database"

  policy =<<EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": [
          "dynamodb:*"
        ],
        "Effect": "Allow",
        "Resource": "arn:aws:dynamodb:*:*:table/UserDB"
      }
    ]
  }
  EOF

}

resource "aws_iam_role_policy_attachment" "attach-dynamodb-ec2" {
  role       = aws_iam_role.role.name
  policy_arn = aws_iam_policy.policy.arn
}