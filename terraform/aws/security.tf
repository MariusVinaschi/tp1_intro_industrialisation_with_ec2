resource "aws_key_pair" "deployer"  {
    key_name = var.myKeyName
    public_key = var.aws_public_key_ssh_path
}

resource "aws_default_security_group" "mySecurityGroup" {
    vpc_id = aws_default_vpc.myVPC.id

    ingress {
        protocol  = 6
        self      = true
        from_port = 22
        to_port   = 22
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        protocol  = 6
        self      = true
        from_port = 80
        to_port   = 80
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = -1
        cidr_blocks = ["0.0.0.0/0"]
    }
}

