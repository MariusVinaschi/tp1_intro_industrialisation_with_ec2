resource "aws_instance" "web" {
    ami = var.ami_id
    instance_type = var.instance_type 
    key_name = var.myKeyName
    tags = {
        Name = var.tag_name
    }
}