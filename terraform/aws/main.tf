terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "3.12.0"
        }
    }
}

provider "aws" {
    profile = "marius"
    region = var.region
}

